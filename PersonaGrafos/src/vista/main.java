package vista;

import controlador.grafo.GrafoNoDirigidoEtiquetado;

/**
 * fecha: 09/02/2023
 *
 * @author: CEAS
 */
public class main {

    public static void main(String[] args) {

        GrafoNoDirigidoEtiquetado gde = new GrafoNoDirigidoEtiquetado(5, String.class);

        gde.etiquetarVertice(1, "Mayuri");//
        gde.etiquetarVertice(2, "Alice");
        gde.etiquetarVertice(3, "Vanessa");
        gde.etiquetarVertice(4, "Letty");//
        gde.etiquetarVertice(5, "Cobos");

        try {
            gde.insertarAristaEtiqueta(gde.obtenerEtiqueta(5), gde.obtenerEtiqueta(2), 10.0f);
            gde.insertarAristaEtiqueta(gde.obtenerEtiqueta(1), gde.obtenerEtiqueta(2), 25.0f);
            gde.insertarAristaEtiqueta(gde.obtenerEtiqueta(3), gde.obtenerEtiqueta(5), 1.0f);
            gde.insertarAristaEtiqueta(gde.obtenerEtiqueta(3), gde.obtenerEtiqueta(4), -10.0f);
            gde.insertarAristaEtiqueta(gde.obtenerEtiqueta(2), gde.obtenerEtiqueta(3), 55.0f);
            gde.insertarAristaEtiqueta(gde.obtenerEtiqueta(1), gde.obtenerEtiqueta(4), 1000.0f);
            //System.out.println(gde.caminiMinimo(1, 4));
           // gde.calcularCaminoMinimo(1, 4).imprimir();
            System.out.println(gde.matrizAdyacencia().length);
        } catch (Exception ex) {
            System.out.println("Error "+ex.getMessage());
            ex.printStackTrace();
        }

    }

}
