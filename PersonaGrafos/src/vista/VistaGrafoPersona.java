package vista;

import controlador.ControladorPersona;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import javax.swing.JOptionPane;
import vista.modelo.ModeloTablaAdyacencia;
import vista.utilidades.Utilidades;

/**
 * fecha: 19/01/2023
 *
 * @author: CEAS
 */
public class VistaGrafoPersona extends javax.swing.JDialog {

    private ControladorPersona persona = new ControladorPersona();
    private ModeloTablaAdyacencia mta = new ModeloTablaAdyacencia();

    public VistaGrafoPersona(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        cargarTabla();
        cargarCombo();
    }

    private void cargarTabla() {
        mta.setGnde(persona.getGnde());
        tblTabla.setModel(mta);
        //actualiza la tabla fireTableStructureChanged()
        mta.fireTableStructureChanged();
        tblTabla.updateUI();
    }

    private void cargarCombo() {
        Utilidades.cargarUbicacion(cbxOrigen);
        Utilidades.cargarUbicacion(cbxDestino);
    }

    private void fijarAdyacencia() {
        Integer inicio = cbxOrigen.getSelectedIndex() + 1;
        Integer destino = cbxDestino.getSelectedIndex() + 1;
        if (inicio.intValue() == destino.intValue()) {
            JOptionPane.showMessageDialog(null, "Escoger ubicaciones diferentes", "AVISO", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                persona.getGnde().insertarAristaEtiqueta(persona.getGnde().obtenerEtiqueta(inicio), persona.getGnde().obtenerEtiqueta(destino), persona.calcularDistancia(inicio, destino));
                cargarTabla();
            } catch (Exception ex) {
                System.out.println("Error " + ex.getMessage());
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnMostrarGrafo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        cbxDestino = new javax.swing.JComboBox<>();
        cbxOrigen = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        btnAdyacencia = new javax.swing.JButton();
        btnAgregarPersona = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtBP = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtBA = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnBusqueda = new javax.swing.JButton();
        cbxCaminoMinimo = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtCaminoMinimo = new javax.swing.JTextArea();
        btnCaminoMinimo = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("VISTA_GRAFO_PERSONA");

        jPanel1.setBackground(new java.awt.Color(255, 255, 153));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("DESTINO");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 70, 80, 20));

        btnMostrarGrafo.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnMostrarGrafo.setText("MOSTRAR GRAFO");
        btnMostrarGrafo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnMostrarGrafo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarGrafoActionPerformed(evt);
            }
        });
        jPanel1.add(btnMostrarGrafo, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 30, -1, -1));

        tblTabla.setForeground(new java.awt.Color(0, 0, 0));
        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 770, 160));

        jPanel1.add(cbxDestino, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 90, 300, 30));

        cbxOrigen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxOrigenActionPerformed(evt);
            }
        });
        jPanel1.add(cbxOrigen, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 300, 30));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("CAMINO MINIMO");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 320, 180, 20));

        btnAdyacencia.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnAdyacencia.setText("ADYACENCIA");
        btnAdyacencia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdyacencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdyacenciaActionPerformed(evt);
            }
        });
        jPanel1.add(btnAdyacencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 90, 110, 30));

        btnAgregarPersona.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnAgregarPersona.setText("AGREGAR PERSONAS");
        btnAgregarPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarPersonaActionPerformed(evt);
            }
        });
        jPanel1.add(btnAgregarPersona, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 30, -1, -1));

        txtBP.setEditable(false);
        txtBP.setColumns(20);
        txtBP.setRows(5);
        jScrollPane2.setViewportView(txtBP);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 340, 170, 100));

        txtBA.setEditable(false);
        txtBA.setColumns(20);
        txtBA.setRows(5);
        jScrollPane3.setViewportView(txtBA);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, 140, 100));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("ORIGEN");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 70, 20));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("BUSQUEDA EN ANCHURA");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 150, 20));

        btnBusqueda.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnBusqueda.setText("BUSQUEDA");
        btnBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBusquedaActionPerformed(evt);
            }
        });
        jPanel1.add(btnBusqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 450, -1, -1));

        cbxCaminoMinimo.setFont(new java.awt.Font("Segoe UI", 3, 12)); // NOI18N
        cbxCaminoMinimo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "FLOYD", "DIJKSTRA" }));
        jPanel1.add(cbxCaminoMinimo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 340, 200, 30));

        txtCaminoMinimo.setEditable(false);
        txtCaminoMinimo.setColumns(20);
        txtCaminoMinimo.setRows(5);
        jScrollPane4.setViewportView(txtCaminoMinimo);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 380, 200, 90));

        btnCaminoMinimo.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnCaminoMinimo.setText("MOSTRAR CAMINO");
        btnCaminoMinimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaminoMinimoActionPerformed(evt);
            }
        });
        jPanel1.add(btnCaminoMinimo, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 390, -1, -1));

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("BUSQUEDA EN PROFUNDIDAD");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 320, 180, 20));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 787, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMostrarGrafoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarGrafoActionPerformed
        new FrmGrafo(null, true, persona.getGnde(), false).setVisible(true);
    }//GEN-LAST:event_btnMostrarGrafoActionPerformed

    private void cbxOrigenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxOrigenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxOrigenActionPerformed

    private void btnAdyacenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdyacenciaActionPerformed
        fijarAdyacencia();
    }//GEN-LAST:event_btnAdyacenciaActionPerformed

    private void btnAgregarPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarPersonaActionPerformed
        new VistaPersona(null, true).setVisible(true);
        cargarTabla();
    }//GEN-LAST:event_btnAgregarPersonaActionPerformed

    private void btnCaminoMinimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaminoMinimoActionPerformed
        Integer inicio = cbxOrigen.getSelectedIndex();
        Integer destino = cbxDestino.getSelectedIndex();

        StringBuilder caminoMinimo = new StringBuilder();
        try {
            if (cbxCaminoMinimo.getSelectedIndex() == 0) {
                ListaEnlazada<Integer> lista = persona.getGnde().floyd(inicio+1, destino+1);
                for (int i = 0; i < lista.getTamanio(); i++) {
                    caminoMinimo.append(persona.getGnde().obtenerEtiqueta(lista.obtenerPosicion(i)) + "\n");
                }
            } else {
                ListaEnlazada<Integer> lista = persona.getGnde().dijkstra(inicio+1, destino+1);
                for (int i = 0; i < lista.getTamanio(); i++) {
                    caminoMinimo.append(persona.getGnde().obtenerEtiqueta(lista.obtenerPosicion(i)) + "\n");
                }
            }
        } catch (ListaNulaException | PosicionNoEncontradaException ex) {
            System.out.println("Error " + ex.getMessage());
        }
        txtCaminoMinimo.setText(caminoMinimo.toString());

    }//GEN-LAST:event_btnCaminoMinimoActionPerformed

    private void btnBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBusquedaActionPerformed
        try {
            StringBuilder BPP = new StringBuilder();
            ListaEnlazada<Integer> lista = persona.getGnde().recorridoProfundidad(1);
            for (int i = 0; i < lista.getTamanio(); i++) {
                BPP.append(persona.getGnde().obtenerEtiqueta(lista.obtenerPosicion(i)) + "\n");
            }
            txtBP.setText(BPP.toString());

            StringBuilder BPA = new StringBuilder();
            ListaEnlazada<Integer> listaAnchura = persona.getGnde().recorridoEnAnchura(1);
            for (int i = 0; i < listaAnchura.getTamanio(); i++) {
                BPA.append(persona.getGnde().obtenerEtiqueta(listaAnchura.obtenerPosicion(i)) + "\n");
            }
            txtBA.setText(BPA.toString());
        } catch (Exception ex) {
            System.out.println("Error " + ex.getMessage());
        }
    }//GEN-LAST:event_btnBusquedaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaGrafoPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaGrafoPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaGrafoPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaGrafoPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaGrafoPersona dialog = new VistaGrafoPersona(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdyacencia;
    private javax.swing.JButton btnAgregarPersona;
    private javax.swing.JButton btnBusqueda;
    private javax.swing.JButton btnCaminoMinimo;
    private javax.swing.JButton btnMostrarGrafo;
    private javax.swing.JComboBox<String> cbxCaminoMinimo;
    private javax.swing.JComboBox<String> cbxDestino;
    private javax.swing.JComboBox<String> cbxOrigen;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable tblTabla;
    private javax.swing.JTextArea txtBA;
    private javax.swing.JTextArea txtBP;
    private javax.swing.JTextArea txtCaminoMinimo;
    // End of variables declaration//GEN-END:variables

}
