package vista;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.view.mxGraph;
import controlador.grafo.Adyacencia;
import controlador.grafo.Grafo;
import controlador.grafo.GrafoNoDirigidoEtiquetado;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import java.awt.Dimension;

/**
 *
 * @author Usuario
 */
public class FrmGrafo extends java.awt.Dialog {

    private Grafo grafo;
    private boolean estado;

    public FrmGrafo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    public FrmGrafo(java.awt.Frame parent, boolean modal, Grafo grafo, Boolean estado) {
        super(parent, modal);
        this.grafo = grafo;
        this.estado = estado;
        initComponents();
        cargarDatos();
    }

    private void cargarDatos() {
        mxGraph graph = new mxGraph();
        mxGraphComponent graphComponent = new mxGraphComponent(graph);

        graphComponent.setSize(new Dimension(600, 600));
        panel.add(graphComponent);

        ListaEnlazada listaInicio = new ListaEnlazada();
        Object padre = graph.getDefaultParent();
        
        for (int i = 1; i <= grafo.numVertices(); i++) {
            Object inicio = null;
            if (estado == true) {
                inicio = graph.insertVertex(padre, String.valueOf(i), String.valueOf(i), 100, 100, 80, 30);
            } else {
                GrafoNoDirigidoEtiquetado gnde = (GrafoNoDirigidoEtiquetado) grafo;
                inicio = graph.insertVertex(padre, gnde.obtenerEtiqueta(i).toString(),
                        gnde.obtenerEtiqueta(i).toString(), 100, 100, 80, 30);
            }
            listaInicio.insertar(inicio);
        }

        try {
            for (int i = 0; i < listaInicio.getTamanio(); i++) {
                ListaEnlazada<Adyacencia> lista = grafo.adyacentes(i+1);
                Object inicio = listaInicio.obtenerPosicion(i);
                for (int j = 0; j < lista.getTamanio(); j++) {
                    Adyacencia a = lista.obtenerPosicion(j);
                    Object destino = listaInicio.obtenerPosicion(a.getDestino() - 1);
                   graph.insertEdge(padre, null, a.getPeso().toString(), inicio, destino);
                }
            }
        } catch (ListaNulaException | PosicionNoEncontradaException e) {
            System.out.println("Error " + e.getMessage());
        } finally {
            graph.getModel().endUpdate();
        }
        morphGraph(graph, graphComponent);
        new mxHierarchicalLayout(graph).execute(graph.getDefaultParent());
    }

    private static void morphGraph(mxGraph graph, mxGraphComponent graphComponent) {
        mxIGraphLayout layaut = new mxFastOrganicLayout(graph);
        graph.getModel().beginUpdate();
        try {
            layaut.execute(graph.getDefaultParent());
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        } finally {
            mxMorphing morph = new mxMorphing(graphComponent, 20, 1.5, 20);
            morph.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
                @Override
                public void invoke(Object o, mxEventObject eo) {
                    graph.getModel().endUpdate();
                }
            });
            morph.startAnimation();
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        panel = new javax.swing.JPanel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        panel.setLayout(null);
        jScrollPane1.setViewportView(panel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(516, 509));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmGrafo dialog = new FrmGrafo(new java.awt.Frame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
