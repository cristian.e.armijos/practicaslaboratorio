package vista.modelo;

import controlador.grafo.GrafoNoDirigidoEtiquetado;
import javax.swing.table.AbstractTableModel;
import modelo.Persona;

/**
 * fecha: 20/01/2023
 *
 * @author: CEAS
 */
public class ModeloTablaAdyacencia extends AbstractTableModel {

    private GrafoNoDirigidoEtiquetado<Persona> gnde;
    private String[] columnas;

    public GrafoNoDirigidoEtiquetado<Persona> getGnde() {
        return gnde;
    }

    public void setGnde(GrafoNoDirigidoEtiquetado<Persona> gnde) {
        this.gnde = gnde;
        generarColumnas();
    }

    @Override
    public int getRowCount() {
        return gnde.getNumVertices();
    }

    @Override
    public int getColumnCount() {
        return gnde.getNumVertices() + 1;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public Object getValueAt(int fila, int columna) {
        if (columna == 0) {
            return columnas[fila+1];
        } else {
            try {
                if (gnde.existeArista(fila+1, columna )) {
                    return gnde.pesoArista(fila+1, columna);
                } else {
                    return "--";
                }
            } catch (Exception ex) {
                System.out.println("Error " + ex.getMessage());
            }
        }
        return "";
    }

    private void generarColumnas() {
        columnas = new String[gnde.numVertices() + 1];
        columnas[0] = "--V--";
        for (int i = 1; i < columnas.length; i++) {
           // System.out.println(gnde.obtenerEtiqueta(i));
            columnas[i] = gnde.obtenerEtiqueta(i).toString();
        }
    }
}
