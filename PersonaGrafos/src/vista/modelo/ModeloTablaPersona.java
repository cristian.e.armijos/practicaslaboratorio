package vista.modelo;

import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Persona;

/**
 * fecha: 01/02/2023
 *
 * @author: CEAS
 */
public class ModeloTablaPersona extends AbstractTableModel {

    private ListaEnlazada<Persona> lista;

    public ListaEnlazada<Persona> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.getTamanio();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "ID";
            case 1:
                return "Nombre";
            case 2:
                return "Genero";
            case 3:
                return "Edad";
            case 4:
                return "Latitud";
            case 5:
                return "Longitud";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int fila, int columna) {
        Persona persona = null;

        try {
            persona = lista.obtenerPosicion(fila);
        } catch (Exception ex) {
            System.out.println("Error " + ex.getMessage());
        }

        switch (columna) {
            case 0:
                return persona != null ? persona.getId() : "";
            case 1:
                return persona != null ? persona.getNombre() : "";
            case 2:
                return persona != null ? persona.getGenero() : "";
            case 3:
                return persona != null ? persona.getEdad() : "";
            case 4:
                return persona != null ? persona.getUbicacion().getLatitud() : "";
            case 5:
                return persona != null ? persona.getUbicacion().getLongitud() : "";
            default:
                return null;
        }
    }

}
