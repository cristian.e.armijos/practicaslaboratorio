package vista.utilidades;

import controlador.ControladorPersona;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import javax.swing.JComboBox;
import modelo.Persona;
import modelo.enums.Genero;

/**
 * fecha: 01/02/2023
 *
 * @author: CEAS
 */
public class Utilidades {

    public static void cargarGeneros(JComboBox cbx) {
        cbx.removeAllItems();
        for (Genero genero : Genero.values()) {
            cbx.addItem(genero);
        }
    }

    public static Genero obtenerGenero(JComboBox cbx) {
        return (Genero) cbx.getSelectedItem();
    }

    public static void cargarUbicacion(JComboBox cbx) {
        try {
            cbx.removeAllItems();
            ControladorPersona persona = new ControladorPersona();
            ListaEnlazada<Persona> lista = persona.listar();
            for (int i = 0; i < lista.getTamanio(); i++) {
                cbx.addItem(lista.obtenerPosicion(i));
            }
        } catch (ListaNulaException | PosicionNoEncontradaException ex) {
            System.out.println("Error " + ex.getMessage());
        }
    }
    
}
