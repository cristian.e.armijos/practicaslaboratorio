package vista;

import controlador.ControladorPersona;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import javax.swing.JOptionPane;
import vista.modelo.ModeloTablaPersona;
import vista.utilidades.Utilidades;

/**
 * fecha: 01/02/2023
 *
 * @author: CEAS
 */
public class VistaPersona extends javax.swing.JDialog {
    
    private ControladorPersona persona = new ControladorPersona();
    private ModeloTablaPersona mtp = new ModeloTablaPersona();
    
    public VistaPersona(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        cargarTabla();
        Utilidades.cargarGeneros(cbxGenero);
        btnActualizar.setEnabled(false);
        btnEliminar.setEnabled(false);
    }
    
    private void cargarTabla() {
        mtp.setLista(persona.getLista());
        tblTabla.setModel(mtp);
        tblTabla.updateUI();
    }
    
    private void guardar() {
        if (txtNombre.getText().trim().isEmpty() || txtLatitud.getText().trim().isEmpty() || txtLongitud.getText().trim().isEmpty()
                || cbxGenero.getSelectedIndex() == -1 || edad.getValue().toString().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos vacios", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        } else {
            int fila = persona.getLista().getTamanio() + 1;
            persona.getPersona().setId(fila);
            persona.getPersona().setEdad(Integer.valueOf(edad.getValue().toString()));
            persona.getPersona().setGenero(Utilidades.obtenerGenero(cbxGenero));
            persona.getPersona().setNombre(txtNombre.getText().trim());
            persona.getPersona().getUbicacion().setIdUbicacion(fila);
            persona.getPersona().getUbicacion().setLatitud(Float.valueOf(txtLatitud.getText().trim()));
            persona.getPersona().getUbicacion().setLongitud(Float.valueOf(txtLongitud.getText().trim()));
            persona.getLista().insertar(persona.getPersona());
            if (persona.guardar(persona.getLista())) {
                JOptionPane.showMessageDialog(null, "GUARDADO CORRECTAMENTE", "OK", JOptionPane.INFORMATION_MESSAGE);
                cargarTabla();
                limpiar();
            }
            
        }
    }
    
    private void limpiar() {
        txtLatitud.setText("");
        txtLongitud.setText("");
        txtNombre.setText("");
        cbxGenero.setSelectedIndex(-1);
        edad.setValue(1);
        persona.setPersona(null);
    }
    
    private void cargarDatos() {
        int fila = tblTabla.getSelectedRow();
        try {
            edad.setValue(persona.getLista().obtenerPosicion(fila).getEdad());
            cbxGenero.setSelectedItem(persona.getLista().obtenerPosicion(fila).getGenero());
            txtNombre.setText(persona.getLista().obtenerPosicion(fila).getNombre());
            txtLatitud.setText(String.valueOf(persona.getLista().obtenerPosicion(fila).getUbicacion().getLatitud()));
            txtLongitud.setText(String.valueOf(persona.getLista().obtenerPosicion(fila).getUbicacion().getLongitud()));
            
        } catch (ListaNulaException | PosicionNoEncontradaException ex) {
            System.out.println("Error " + ex.getMessage());
        }
    }
    
    private void actualizar() {
        int fila = tblTabla.getSelectedRow();
        try {
            if (txtNombre.getText().trim().isEmpty() || txtLatitud.getText().trim().isEmpty() || txtLongitud.getText().trim().isEmpty()
                    || cbxGenero.getSelectedIndex() == -1 || edad.getValue().toString().trim().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Campos vacios", "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
            } else {
                persona.getLista().obtenerPosicion(fila).setEdad(Integer.valueOf(edad.getValue().toString()));
                persona.getLista().obtenerPosicion(fila).setGenero(Utilidades.obtenerGenero(cbxGenero));
                persona.getLista().obtenerPosicion(fila).setNombre(txtNombre.getText().trim());
                persona.getLista().obtenerPosicion(fila).getUbicacion().setLatitud(Float.valueOf(txtLatitud.getText().trim()));
                persona.getLista().obtenerPosicion(fila).getUbicacion().setLongitud(Float.valueOf(txtLongitud.getText().trim()));
                if (persona.modificar(persona.getLista())) {
                    JOptionPane.showMessageDialog(null, "Modificado correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
                    cargarTabla();
                    limpiar();
                }
            }
        } catch (ListaNulaException | PosicionNoEncontradaException ex) {
            System.out.println("Error " + ex.getMessage());
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        edad = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        cbxGenero = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtLatitud = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtLongitud = new javax.swing.JTextField();
        btnActualizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnGuardar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(153, 255, 204));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("GENERO");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, -1, -1));

        txtNombre.setBackground(new java.awt.Color(255, 255, 255));
        txtNombre.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 260, 30));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("NOMBRE");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        edad.setModel(new javax.swing.SpinnerNumberModel(1, 1, 130, 10));
        jPanel1.add(edad, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 30, -1, 30));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("EDAD");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, -1, -1));

        cbxGenero.setBackground(new java.awt.Color(255, 255, 255));
        cbxGenero.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(cbxGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 30, 170, 30));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("LATITUD");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        txtLatitud.setBackground(new java.awt.Color(255, 255, 255));
        txtLatitud.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(txtLatitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 260, 30));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("LONGITUD");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, -1, -1));

        txtLongitud.setBackground(new java.awt.Color(255, 255, 255));
        txtLongitud.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(txtLongitud, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 120, 270, 30));

        btnActualizar.setBackground(new java.awt.Color(204, 204, 0));
        btnActualizar.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnActualizar.setForeground(new java.awt.Color(0, 0, 0));
        btnActualizar.setText("ACTUALIZAR");
        btnActualizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });
        jPanel1.add(btnActualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 170, 110, 30));

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblTabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblTabla);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 560, 180));

        btnGuardar.setBackground(new java.awt.Color(204, 204, 0));
        btnGuardar.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(0, 0, 0));
        btnGuardar.setText("GUARDAR");
        btnGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel1.add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(229, 172, 90, 30));

        btnEliminar.setBackground(new java.awt.Color(204, 204, 0));
        btnEliminar.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnEliminar.setForeground(new java.awt.Color(0, 0, 0));
        btnEliminar.setText("ELIMINAR");
        btnEliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jPanel1.add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 170, 90, 30));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        int fila = tblTabla.getSelectedRow();
        if (fila != -1) {
            actualizar();
            btnActualizar.setEnabled(false);
            btnGuardar.setEnabled(true);
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione la persona", "AVISO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void tblTablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTablaMouseClicked
        if (evt.getClickCount() == 2) {
            cargarDatos();
            btnActualizar.setEnabled(true);
            btnGuardar.setEnabled(false);
            btnEliminar.setEnabled(false);
        } else if (evt.getClickCount() == 1) {
            btnEliminar.setEnabled(true);
        }
    }//GEN-LAST:event_tblTablaMouseClicked

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        guardar();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int fila = tblTabla.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(null, "Seleccione una fila", "AVISO", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                int resp = JOptionPane.showConfirmDialog(null, "¿ESTA SEGURO DE ELIMINAR ESTA PERSONA?", "INFORMACION", JOptionPane.OK_CANCEL_OPTION);
                if (resp == 0) {
                    persona.eliminar(fila);
                    JOptionPane.showMessageDialog(null, "Eliminado con exito", "INFO", JOptionPane.INFORMATION_MESSAGE);
                    cargarTabla();
                    btnEliminar.setEnabled(false);
                }
            } catch (Exception ex) {
                System.out.println("Error " + ex.getMessage());
            }
        }
    }//GEN-LAST:event_btnEliminarActionPerformed
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaPersona dialog = new VistaPersona(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JComboBox<String> cbxGenero;
    private javax.swing.JSpinner edad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTabla;
    private javax.swing.JTextField txtLatitud;
    private javax.swing.JTextField txtLongitud;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables

}
