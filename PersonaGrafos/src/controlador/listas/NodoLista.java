package controlador.listas;

/**
 * fecha: 9 nov 2022
 * @author: CEAS 
 */
public class NodoLista <E>{
    //E por elemento
    private E dato;
    private NodoLista<E> puntero;
    
    public NodoLista(E dato, NodoLista<E> apuntador){
        this.dato=dato;
        this.puntero=apuntador;
    }

    public E getDato() {
        return dato;
    }

    public void setDato(E dato) {
        this.dato = dato;
    }

    public NodoLista<E> getApuntador() {
        return puntero;
    }

    public void setApuntador(NodoLista<E> apuntador) {
        this.puntero = apuntador;
    }
    
    
}
