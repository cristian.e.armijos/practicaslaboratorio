package controlador.listas;

import controlador.listas.excepciones.AtributoException;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import controlador.utiles.Utilidades;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

/**
 * fecha: 9 nov 2022
 *
 * @author: CEAS
 */
public class ListaEnlazada<E> {

    //lista: Colección de nodos = elementos = dato y apuntador
    private NodoLista<E> cabecera;
    private Integer tamanio;
    public static Integer ASCENDENTE = 1;
    public static Integer DESCENDENTE = 2;

    //crear lista
    public ListaEnlazada() {
        cabecera = null;
        tamanio = 0;
    }

    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    public Integer getTamanio() {
        return tamanio;
    }

    public void setTamanio(Integer tamanio) {
        this.tamanio = tamanio;
    }

    //lista esta vacia
    public Boolean estaVacia() {
        return cabecera == null;
    }

    //insertar datos al final
    public void insertar(E dato) {
        NodoLista<E> nodo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            this.cabecera = nodo;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getApuntador() != null) {
                aux = aux.getApuntador();
            }
            aux.setApuntador(nodo);
        }
        tamanio++;
    }

    //imprime datos
    public void imprimir() {
        System.out.println("------------------LISTA ENLAZADA-------------------");
        NodoLista<E> aux = cabecera;
        while (aux != null) {
            System.out.print(aux.getDato().toString() + "\t");
            aux = aux.getApuntador();
        }
        System.out.println("\n=====================================================");
    }

    //insertar datos al inicio de la lista
    public void insertarCabecera(E dato) {
        NodoLista<E> nodo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            insertar(dato);
        } else {
            nodo.setApuntador(cabecera);
            cabecera = nodo;
            tamanio++;
        }
    }

    public void insertarPosicion(E dato, Integer pos) throws PosicionNoEncontradaException {
        if (estaVacia()) {
            insertar(dato);
        } else if (pos >= 0 && pos < tamanio) {
            NodoLista<E> nodo = new NodoLista<>(dato, null);
            NodoLista<E> aux = cabecera;
            if (pos == (tamanio - 1)) {
                insertar(dato);
            } else if (pos == 0) {
                insertarCabecera(dato);
            } else {

                for (int i = 0; i < pos - 1; i++) {
                    aux = aux.getApuntador();
                }
                NodoLista<E> siguiente = aux.getApuntador();
                aux.setApuntador(nodo);
                nodo.setApuntador(siguiente);
                tamanio++;
            }
        } else {
            throw new PosicionNoEncontradaException();
        }
    }

    public E obtenerPosicion(Integer pos) throws ListaNulaException, PosicionNoEncontradaException {
        E dato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < tamanio) {
                if (pos == 0) {
                    dato = cabecera.getDato();

                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getApuntador();
                    }
                    dato = aux.getDato();
                }
            } else {
                throw new PosicionNoEncontradaException();
            }
            return dato;
        } else {
            throw new ListaNulaException();
        }

    }

    public E eliminarDato(Integer pos) throws Exception {
        E dato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < tamanio) {
                if (pos == 0) {
                    dato = cabecera.getDato();
                    cabecera = cabecera.getApuntador();
                    tamanio--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getApuntador();
                    }
                    dato = aux.getDato();
                    NodoLista<E> proximo = aux.getApuntador();
                    aux.setApuntador(proximo.getApuntador());
                    tamanio--;
                }
            } else {
                throw new PosicionNoEncontradaException();
            }
            return dato;
        } else {
            throw new ListaNulaException();
        }
    }

    //método para transformar lista a un arreglo
    public E[] toArray() {
        E[] arreglo = null;
        if (tamanio > 0) {
            arreglo = (E[]) Array.newInstance(cabecera.getDato().getClass(), tamanio);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < tamanio; i++) {
                arreglo[i] = aux.getDato();
                aux = aux.getApuntador();
            }
        }
        return arreglo;
    }

    //método para convertir un arreglo en una lista enlazada
    public ListaEnlazada<E> toLista(E[] arreglo) {
        vaciarLista();
        for (int i = 0; i < arreglo.length; i++) {
            insertar(arreglo[i]);
        }
        return this;
    }

    public void vaciarLista() {
        cabecera = null;
        tamanio = 0;
    }

    public ListaEnlazada<E> quickSort(ListaEnlazada<E> lista, String atributo) throws ListaNulaException, PosicionNoEncontradaException, IllegalArgumentException, IllegalAccessException, AtributoException {
        if (lista.getTamanio() > 1) {
            E[] arreglo = lista.toArray();
            quickSort(arreglo, 0, arreglo.length - 1, atributo);
            return toLista(arreglo);
        }
        return lista;
    }

    private void quickSort(E[] arreglo, int inicio, int fin, String atributo) throws AtributoException, IllegalArgumentException, IllegalAccessException {
        if (inicio < fin) {
            E pivote = arreglo[(inicio + fin) / 2];
            int i = inicio;
            int j = fin;
            Field field = Utilidades.obtenerAtributo(pivote.getClass(), atributo);
            field.setAccessible(true);
            if (field == null) {
                throw new AtributoException();
            }
            Object elemento = field.get(pivote);
            while (i <= j) {
                while (i <= j) {
                    Object elemento1 = field.get(arreglo[i]);
                    if (compararDatos(elemento1, elemento)) {
                        i++;
                    }
                    break;
                }
                while (i <= j) {
                    Object elemento1 = field.get(arreglo[j]);
                    if (compararDatos(elemento, elemento1)) {
                        j--;
                    }
                    break;
                }
                if (i <= j) {
                    E aux = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = aux;
                    i++;
                    j--;
                }
            }
            if (inicio < j) {
                quickSort(arreglo, inicio, j, atributo);
            }
            if (i < fin) {
                quickSort(arreglo, i, fin, atributo);
            }
        }
    }

    private Boolean compararDatos(Object dato, Object dato1) throws AtributoException, IllegalArgumentException, IllegalAccessException {
        if (Utilidades.isNumber(dato.getClass())) {
            if (((Number) dato).floatValue() < ((Number) dato1).floatValue()) {
                return true;
            }
        } else if (Utilidades.isString(dato.getClass())) {
            if (dato.toString().compareToIgnoreCase(dato1.toString()) < 0) {
                return true;
            }
        }
        return false;
    }

    public ListaEnlazada<E> secuencial(String atributo, Object dato) throws Exception {
        Class<E> clazz = null;
        ListaEnlazada<E> lista = new ListaEnlazada<>();
        if (tamanio > 0) {
            clazz = (Class<E>) cabecera.getDato().getClass();
            Boolean esObjeto = Utilidades.isObject(clazz);
            E[] arreglo = toArray();
            for (int i = 0; i < arreglo.length; i++) {
                if (esObjeto) {
                    Boolean bandera = buscarPosicionObjeto(arreglo[i], dato, clazz, atributo);
                    if (bandera) {
                        lista.insertar(arreglo[i]);
                    }
                } else {
                    Boolean bandera = buscarPosicionPrimitivo(arreglo[i], dato);
                    if (bandera) {
                        lista.insertar(arreglo[i]);
                    }
                }
            }
        }
        return lista;
    }

    private Boolean buscarPosicionPrimitivo(Object datoArreglo, Object buscar) {
        if (Utilidades.isNumber(buscar.getClass())) {
            if (((Number) datoArreglo).floatValue() == ((Number) buscar).floatValue()) {
                return true;
            }
        } else if (Utilidades.isString(buscar.getClass())) {
            if (datoArreglo.toString().toLowerCase().startsWith(buscar.toString())
                    || datoArreglo.toString().toLowerCase().endsWith(buscar.toString())
                    || datoArreglo.toString().toLowerCase().contains(buscar.toString())
                    || datoArreglo.toString().equalsIgnoreCase(buscar.toString())) {
                return true;
            }
        }
        return false;
    }

    private Boolean buscarPosicionObjeto(E objeto, Object buscar, Class clazz, String atributo) throws Exception {
        Field field = Utilidades.obtenerAtributo(clazz, atributo);
        if (field == null) {
            throw new AtributoException();
        } else {
            field.setAccessible(true);
            Object aux = field.get(objeto);
            return buscarPosicionPrimitivo(aux, buscar);
        }
    }

    public ListaEnlazada<E> secuencialBinaria(Object valor, String atributo) throws Exception {
        ListaEnlazada<E> lista = new ListaEnlazada<>();
        lista = binaria(valor, atributo);
        if (lista.getTamanio() == 1) {
            return lista;
        } else {
            return secuencial(atributo, valor);
        }
    }

    public ListaEnlazada<E> binaria(Object valor, String atributo) throws ListaNulaException, PosicionNoEncontradaException, Exception {
        ListaEnlazada<E> lista = new ListaEnlazada<>();

        if (tamanio > 0) {
            int inicio = 0;
            int fin = getTamanio();
            E[] arreglo = toArray();
            while (inicio <= fin) {
                int medio = (inicio + fin) / 2;
                Object elementoMedio = arreglo[medio];
                Field field = Utilidades.obtenerAtributo(elementoMedio.getClass(), atributo);
                if (field == null) {
                    throw new AtributoException();
                }
                field.setAccessible(true);
                Object elemento = field.get(elementoMedio);
                if (elemento.toString().equalsIgnoreCase(valor.toString())) {
                    lista.insertar(arreglo[medio]);
                    break;
                } else if (elemento.toString().compareTo(valor.toString()) < 0) {
                    inicio = medio + 1;
                } else {
                    fin = medio - 1;
                }
            }
        }
        return lista;
    }

}
