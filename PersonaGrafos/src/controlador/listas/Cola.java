package controlador.listas;

import controlador.listas.excepciones.CimaException;
import controlador.listas.excepciones.ColaVaciaException;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.PosicionNoEncontradaException;

/**
 * fecha: 11 nov 2022
 * @author: CEAS 
 */
public class Cola <E> extends ListaEnlazada<E> {
    private Integer cima;
    
    public Cola(Integer cima){
        this.cima=cima;
    }
    public boolean estaLlena() {
        return cima == getTamanio();
    }
    public void queue(E dato) throws CimaException, PosicionNoEncontradaException{
        if(!estaLlena()){
            if(getTamanio()==0){
                insertar(dato);
                insertarPosicion(dato, getTamanio() -1);
            }
        }else throw new CimaException();
    }
    
    public E dequeue() throws ColaVaciaException, PosicionNoEncontradaException, Exception{
        if(!estaVacia()){
            E dato = eliminarDato(0) ;
            return dato;
        }else throw  new ColaVaciaException();
    }
}
