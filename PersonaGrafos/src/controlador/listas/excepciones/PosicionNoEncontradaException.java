package controlador.listas.excepciones;

/**
 * fecha: 9 nov 2022
 *
 * @author: CEAS
 */
public class PosicionNoEncontradaException extends Exception {

    public PosicionNoEncontradaException(String msg) {
        super(msg);
    }

    public PosicionNoEncontradaException() {
        super("La posicion esta fuera de los limites");
    }
    
    
}
