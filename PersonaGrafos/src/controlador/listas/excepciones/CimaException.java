package controlador.listas.excepciones;

/**
 * fecha: 11 nov 2022
 * @author: CEAS 
 */
public class CimaException extends Exception{

    public CimaException() {
        super("cima alcanzada");
    }

    public CimaException(String msg) {
        super(msg);
    }
}
