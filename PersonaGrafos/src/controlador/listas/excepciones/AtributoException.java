package controlador.listas.excepciones;

/**
 * fecha: 13/12/2022
 * @author: CEAS 
 */
public class AtributoException extends Exception {

    public AtributoException() {
        super("No se puede encontrar el atributo dado");
    }

    public AtributoException(String msg) {
        super(msg);
    }
}
