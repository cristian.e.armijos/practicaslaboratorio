package controlador.listas.excepciones;

/**
 * fecha: 11 nov 2022
 * @author: CEAS 
 */
public class ColaVaciaException extends Exception {

    public ColaVaciaException() {
        super("pinshi cola vacia");
    }

    public ColaVaciaException(String msg) {
        super(msg);
    }
}
