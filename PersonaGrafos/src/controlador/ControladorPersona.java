package controlador;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import controlador.grafo.GrafoNoDirigidoEtiquetado;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import controlador.utiles.Utilidades;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import modelo.Persona;

/**
 * fecha: 29/01/2023
 *
 * @author: CEAS
 */
public class ControladorPersona {

    private Persona persona;
    private ListaEnlazada<Persona> lista;
    private GrafoNoDirigidoEtiquetado<Persona> gnde;

    public GrafoNoDirigidoEtiquetado<Persona> getGnde() {
        if (gnde == null) {
            crearGrafo();
        }
        return gnde;
    }

    public void setGnde(GrafoNoDirigidoEtiquetado<Persona> gnde) {
        this.gnde = gnde;
    }

    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public ListaEnlazada<Persona> getLista() {
        if (lista == null) {
            lista = listar();
        }
        return lista;
    }

    public void setLista(ListaEnlazada<Persona> lista) {
        this.lista = lista;
    }

    public Boolean guardar(ListaEnlazada<Persona> lista) {
        FileWriter file = null;
        try {
            file = new FileWriter("Datos" + File.separatorChar + getPersona().getClass().getSimpleName() + ".json");
            file.write(new GsonBuilder().setPrettyPrinting().create().toJson(lista));
            file.close();
            return true;
        } catch (IOException ex) {
            System.out.println("Error " + ex.getMessage());
            return false;
        }

    }

    public ListaEnlazada<Persona> listar() {
        FileReader file = null;
        try {
            file = new FileReader("Datos" + File.separatorChar + getPersona().getClass().getSimpleName() + ".json");
            Type tipo = new TypeToken<ListaEnlazada<Persona>>() {
            }.getType();
            lista = new Gson().fromJson(file, tipo);
            file.close();
        } catch (IOException ex) {
            System.out.println("Error " + ex.getMessage());
        }
        return lista;
    }

    public Boolean eliminar(Integer fila) throws Exception {
        lista = listar();
        lista.eliminarDato(fila);
        return guardar(lista);
    }

    public Boolean modificar(ListaEnlazada<Persona> lista) {
        return guardar(lista);
    }

    private void crearGrafo() {
        gnde = new GrafoNoDirigidoEtiquetado<>(listar().getTamanio(), Persona.class);
        for (int i = 0; i < listar().getTamanio(); i++) {
            try {
                gnde.etiquetarVertice(i + 1, listar().obtenerPosicion(i));
            } catch (ListaNulaException | PosicionNoEncontradaException ex) {
                System.out.println("Error " + ex.getMessage());
            }
        }
    }

    public Float calcularDistancia(Integer origen, Integer destino) {
        Persona o = gnde.obtenerEtiqueta(origen);
        Persona d = gnde.obtenerEtiqueta(destino);
        return Utilidades.calcularDistancia(o.getUbicacion().getLatitud(), d.getUbicacion().getLatitud(), o.getUbicacion().getLongitud(), d.getUbicacion().getLongitud());
    }
    /*     public static void main(String[] args) {
        try {
            Persona persona = new Persona();
            persona.setEdad(52);
            persona.setGenero(Genero.OTRO);
            persona.setId(1);
            persona.setNombre("Juan Torres");
            persona.getUbicacion().setIdUbicacion(1);
            persona.getUbicacion().setLongitud(10.5f);
            persona.getUbicacion().setLatitud(0.56f);
            ListaEnlazada<Persona> listaEnlazada = new ListaEnlazada();
            listaEnlazada.insertar(persona);
            ControladorPersona controladorPersona = new ControladorPersona();
            controladorPersona.guardar(listaEnlazada);
            controladorPersona.listar().imprimir();
        } catch (Exception ex) {
            System.out.println("Error " + ex.getMessage());
        }
    }*/
}
