package controlador.grafo;

/**
 *
 * @author Usuario
 */
public class Adyacencia {
    private Integer destino;
    private Float peso;

    public Adyacencia(Integer destino, Float peso) {
        this.destino = destino;
        this.peso = peso;
    }

    public Integer getDestino() {
        return destino;
    }

    public void setDestino(Integer destino) {
        this.destino = destino;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    @Override
    public String toString() {
        return "destino=" + destino + ", peso=" + peso;
    }   
}
