package controlador.grafo;

import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;

/**
 *
 * @author Usuario
 */
public abstract class Grafo {
    // VERTICES Y ARISTAS 

    public abstract Integer numVertices();

    public abstract Integer numAristas();

    public abstract Boolean existeArista(Integer origen, Integer destino) throws Exception;

    public abstract Float pesoArista(Integer origen, Integer destino);

    public abstract void insertarArista(Integer origen, Integer destino) throws Exception;

    public abstract void insertarArista(Integer origen, Integer destino, Float peso) throws Exception;

    public abstract ListaEnlazada<Adyacencia> adyacentes(Integer vertive);

    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder("");
        try {
            for (int i = 1; i <= numVertices(); i++) {
                grafo.append("Vertice " + String.valueOf(i));
                ListaEnlazada<Adyacencia> lista = adyacentes(i);
                for (int j = 0; j < lista.getTamanio(); j++) {
                    Adyacencia a = lista.obtenerPosicion(j);
                    if (a.getPeso().toString().equalsIgnoreCase(String.valueOf(Float.NaN))) {
                        grafo.append("--------Vertice destino " + a.getDestino() + "---SP");
                    } else {
                        grafo.append("--------Vertice destino " + a.getDestino() + "---Peso" + a.getPeso());

                    }
                }
                grafo.append("\n");
            }
        } catch (Exception e) {
            grafo.append(e.getMessage());
        }

        return grafo.toString();
    }

    public ListaEnlazada calcularCaminoMinimo(Integer origen, Integer destino) throws Exception {
        ListaEnlazada camino = new ListaEnlazada();
        if (estaConectado()) {
            ListaEnlazada pesos = new ListaEnlazada();
            Boolean finalizar = false;
            Integer inicial = origen;
            camino.insertar(inicial);
            while (!finalizar) {
                ListaEnlazada<Adyacencia> adyacencias = adyacentes(inicial);
                Float peso = Float.POSITIVE_INFINITY;
                int T = -1;
                for (int i = 0; i < adyacencias.getTamanio(); i++) {
                    Adyacencia adyacencia = adyacencias.obtenerPosicion(i);
                    if (!estaEnCamino(camino, destino)) {
                        Float pesoArista = adyacencia.getPeso();
                        if (destino.intValue() == adyacencia.getDestino().intValue()) {
                            T = adyacencia.getDestino();
                            peso = pesoArista;
                            break;
                        } else if (pesoArista < peso) {
                            T = adyacencia.getDestino();
                            peso = pesoArista;
                        }
                    }
                }
                pesos.insertar(peso);
                camino.insertar(T);
                inicial = T;
                if (destino.intValue() == inicial.intValue()) {
                    finalizar = true;
                }
            }
        } else {
            throw new Exception("GRAFO NO CONECTADO");
        }
        return camino;
    }

    public Boolean estaConectado() {
        Boolean bandera = true;

        for (int i = 0; i <= numVertices(); i++) {
            ListaEnlazada<Adyacencia> lista = adyacentes(i);
            if (lista.estaVacia() || lista.getTamanio() == 0) {
                bandera = false;
                break;
            }
        }
        return bandera;
    }

    public Boolean estaEnCamino(ListaEnlazada<Integer> lista, Integer vertice) throws ListaNulaException, PosicionNoEncontradaException {
        Boolean band = false;

        for (int i = 0; i < lista.getTamanio(); i++) {
            if (lista.obtenerPosicion(i).intValue() == vertice.intValue()) {
                band = true;
                break;
            }
        }
        return band;
    }

    public ListaEnlazada<Integer> dijkstra(int inicio, int destino) {
        Integer n = numVertices()+1;
        boolean[] visitados = new boolean[n];
        Float[] distancias = new Float[n];
        Integer[] predecesores = new Integer[n];

        // Inicializar las distancias con infinito y los predecesores con -1
        for (int i = 0; i < n; i++) {
            distancias[i] = Float.POSITIVE_INFINITY;
            predecesores[i] = -1;
        }

        // La distancia al nodo inicio es 0
        distancias[inicio] = 0f;

        // Recorrer todos los nodos
        for (int i = 0; i < n; i++) {
            // Encontrar el nodo no visitado con menor distancia
            int u = -1;
            float minDist = Float.POSITIVE_INFINITY;
            for (int j = 0; j < n; j++) {
                if (!visitados[j] && distancias[j] < minDist) {
                    u = j;
                    minDist = distancias[j];
                }
            }

            // Si no se encontró un nodo no visitado con menor distancia, terminar
            if (u == -1) {
                break;
            }

            // Marcar el nodo como visitado
            visitados[u] = true;

            // Actualizar las distancias y los predecesores de los nodos adyacentes
            for (int v = 0; v < n; v++) {
                try {
                    if (existeArista(u, v)) {
                        float resultado = distancias[u] + pesoArista(u, v);
                        if (resultado < distancias[v]) {
                            distancias[v] = resultado;
                            predecesores[v] = u;
                        }
                    }
                } catch (Exception ex) {
                    System.out.println("Error " + ex.getMessage());
                }
            }
        }

        // Construir la lista con el camino mínimo
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        int vertice = destino;
        while (vertice != -1) {
            camino.insertarCabecera(vertice);
            vertice = predecesores[vertice];
        }

        return camino;
    }

}
