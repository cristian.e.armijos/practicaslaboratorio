package controlador.grafo;

import controlador.grafo.excepcion.VerticeOfSizeException;
import controlador.grafo.excepcion.VerticesIgualesException;

/**
 *
 * @author Usuario
 */
public class GrafoNoDirigido extends GrafoDirigido {

    public GrafoNoDirigido(Integer numV) {
        super(numV);
    }

    @Override
    public void insertarArista(Integer origen, Integer destino, Float peso) throws Exception {
        if (origen.intValue() != destino.intValue()) {
            if (origen <= getNumVertices() && destino <= getNumVertices()) {
                if (!existeArista(origen, destino)) {
                    setNumAristas(getNumAristas() + 1);
                    getListaAdyacente()[origen].insertar(new Adyacencia(destino, peso));
                    getListaAdyacente()[destino].insertar(new Adyacencia(origen, peso));
                }
            } else {
                throw new VerticeOfSizeException();
            }
        } else {
            throw new VerticesIgualesException();
        }
    }
}
