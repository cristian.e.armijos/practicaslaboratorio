package controlador.grafo.excepcion;

/**
 *
 * @author Usuario
 */
public class VerticeOfSizeException extends Exception{

    public VerticeOfSizeException() {
        super("No se puede sobrepasar el numero de vertices");
    }

    public VerticeOfSizeException(String message) {
        super(message);
    }
    
}
