package controlador.grafo.excepcion;

/**
 * fecha: 14/01/2023
 * @author: CEAS 
 */
public class VerticesIgualesException extends Exception {

    public VerticesIgualesException() {
        super("Vertices iguales, no se pueden insertar");
    }

    public VerticesIgualesException(String message) {
        super(message);
    }

}
