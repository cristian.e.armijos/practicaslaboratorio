package controlador.grafo;

import controlador.grafo.excepcion.VerticeOfSizeException;
import controlador.grafo.excepcion.VerticesIgualesException;
import controlador.listas.Cola;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;

/**
 *
 * @author Usuario
 */
public class GrafoDirigido extends Grafo {

    private Integer numVertices;
    private Integer numAristas;
    private ListaEnlazada<Adyacencia> listaAdyacente[];

    public GrafoDirigido(Integer numVertices) {
        this.numVertices = numVertices;
        numAristas = 0;
        listaAdyacente = new ListaEnlazada[numVertices + 1];
        for (int i = 1; i <= this.numVertices; i++) {
            listaAdyacente[i] = new ListaEnlazada<>();
        }
    }

    @Override
    public Integer numVertices() {
        return numVertices;
    }

    @Override
    public Integer numAristas() {
        return numAristas;
    }

    @Override
    public Boolean existeArista(Integer origen, Integer destino) throws Exception {
        Boolean existe = false;
        if (origen <= numVertices && destino <= numVertices) {
            ListaEnlazada<Adyacencia> lista = listaAdyacente[origen];
            for (int i = 0; i < lista.getTamanio(); i++) {
                Adyacencia a = lista.obtenerPosicion(i);
                if (a.getDestino().intValue() == destino.intValue()) {
                    existe = true;
                    break;
                }
            }
        } else {
            throw new VerticeOfSizeException();
        }
        return existe;
    }

    @Override
    public Float pesoArista(Integer origen, Integer destino) {
        Float peso = Float.NaN; //NaN--->No es un valor numerico
        try {
            if (existeArista(origen, destino)) {
                ListaEnlazada<Adyacencia> adyacentes = listaAdyacente[origen];
                for (int i = 0; i < adyacentes.getTamanio(); i++) {
                    Adyacencia a = adyacentes.obtenerPosicion(i);
                    if (a.getDestino() == destino.intValue()) {
                        peso = a.getPeso();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer origen, Integer destino, Float peso) throws Exception {
        try {
            if (origen.intValue() != destino) {
                if (origen <= numVertices && destino <= numVertices) {
                    if (!existeArista(origen, destino)) {
                        numAristas++;
                        listaAdyacente[origen].insertar(new Adyacencia(destino, peso));
                    }
                } else {
                    throw new VerticeOfSizeException();
                }
            } else {
                throw new VerticesIgualesException();
            }
        } catch (Exception e) {
            System.out.println("Error" + e);
        }

    }

    @Override
    public void insertarArista(Integer origen, Integer destino) throws Exception {
        insertarArista(origen, destino, Float.NaN);
    }

    @Override
    public ListaEnlazada<Adyacencia> adyacentes(Integer vertice) {

        return listaAdyacente[vertice];
    }

    public Integer getNumVertices() {
        return numVertices;
    }

    public void setNumVertices(Integer numVertices) {
        this.numVertices = numVertices;
    }

    public Integer getNumAristas() {
        return numAristas;
    }

    public void setNumAristas(Integer numAristas) {
        this.numAristas = numAristas;
    }

    public ListaEnlazada<Adyacencia>[] getListaAdyacente() {
        return listaAdyacente;
    }

    public ListaEnlazada<Integer> recorridoProfundidad(Integer inicio) {
        ListaEnlazada<Integer> visitados = new ListaEnlazada<>();
        try {
            boolean[] marcados = new boolean[numVertices + 1];
            recorridoProfundidadAux(inicio, visitados, marcados);
        } catch (ListaNulaException | PosicionNoEncontradaException ex) {
            System.out.println("Error " + ex.getMessage());
        }
        return visitados;
    }

    private void recorridoProfundidadAux(Integer v, ListaEnlazada<Integer> visitados, boolean[] marcados) throws ListaNulaException, PosicionNoEncontradaException {
        marcados[v] = true;
        visitados.insertar(v);
        ListaEnlazada<Adyacencia> adyacentes = adyacentes(v);
        for (int i = 0; i < adyacentes.getTamanio(); i++) {
            Adyacencia a = adyacentes.obtenerPosicion(i);
            Integer w = a.getDestino();
            if (!marcados[w]) {
                recorridoProfundidadAux(w, visitados, marcados);
            }
        }
    }

    public ListaEnlazada<Integer> recorridoEnAnchura(Integer vertice) {
        ListaEnlazada<Integer> visitados = new ListaEnlazada<>();
        try {
            boolean[] visitado = new boolean[numVertices + 1];
            Cola<Integer> cola = new Cola<>(numVertices);

            visitado[vertice] = true;
            visitados.insertar(vertice);
            cola.queue(vertice);

            while (!cola.estaVacia()) {
                Integer v = cola.dequeue();
                ListaEnlazada<Adyacencia> adyacentes = listaAdyacente[v];
                for (int i = 0; i < adyacentes.getTamanio(); i++) {
                    Adyacencia a = adyacentes.obtenerPosicion(i);
                    Integer destino = a.getDestino();
                    if (!visitado[destino]) {
                        visitado[destino] = true;
                        visitados.insertar(destino);
                        cola.queue(destino);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("Error " + ex.getMessage());
        }
        return visitados;
    }

    public Float[][] matrizAdyacencia() throws Exception {
        Float[][] pesos = new Float[numVertices()][numVertices()];
        for (int i = 0; i < numVertices(); i++) {
            for (int j = 0; j < numVertices(); j++) {
                if (i == j) {
                    pesos[i][j] = 0f;
                } else if (!existeArista(i, j)) {
                    pesos[i][j] = Float.POSITIVE_INFINITY;
                } else {
                    pesos[i][j] = pesoArista(i, j);
                }
            }
        }
        return pesos;
    }

    public ListaEnlazada<Integer> floyd(int origen, int destino) {
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        try {
            Float matrizAdyacencia[][] = matrizAdyacencia();

            Float[][] distancias = new Float[numVertices() + 1][numVertices() + 1];
            Integer[][] predecesores = new Integer[numVertices() + 1][numVertices() + 1];

            for (int i = 1; i <= numVertices(); i++) {
                for (int j = 1; j <= numVertices(); j++) {
                    if (i == j) {
                        distancias[i][j] = 0.0f;
                        predecesores[i][j] = i;
                    } else if (matrizAdyacencia[i][j] != null) {
                        distancias[i][j] = matrizAdyacencia[i][j];
                        predecesores[i][j] = i;
                    } else {
                        distancias[i][j] = Float.POSITIVE_INFINITY;
                        predecesores[i][j] = null;
                    }
                }
            }

            for (int k = 1; k <= numVertices(); k++) {
                for (int i = 1; i <= numVertices(); i++) {
                    for (int j = 1; j <= numVertices(); j++) {
                        if (distancias[i][j] > distancias[i][k] + distancias[k][j]) {
                            distancias[i][j] = distancias[i][k] + distancias[k][j];
                            predecesores[i][j] = predecesores[k][j];
                        }
                    }
                }
            }

            if (predecesores[origen][destino] == null) {
                return camino; // No hay camino
            }

            camino.insertarCabecera(destino);
            int i = origen;
            while (i != destino) {
                i = predecesores[i][destino];
                camino.insertarCabecera(i);
            }

        } catch (Exception ex) {
            System.out.println("Error " + ex.getMessage());
            ex.printStackTrace();
        }
        return camino;
    }

}
