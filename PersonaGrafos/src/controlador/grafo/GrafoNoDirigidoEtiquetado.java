package controlador.grafo;

import controlador.listas.ListaEnlazada;
import java.lang.reflect.Array;
import java.util.HashMap;

/**
 * fecha: 13/01/2023
 *
 * @author: CEAS
 */
public class GrafoNoDirigidoEtiquetado<E> extends GrafoNoDirigido {

    protected E etiquetas[];
    protected HashMap<E, Integer> dicVertices;
    private Class<E> clazz;

    public GrafoNoDirigidoEtiquetado(Integer numVertices, Class clazz) {
        super(numVertices);
        this.clazz = clazz;
        etiquetas = (E[]) Array.newInstance(clazz, numVertices + 1);
        dicVertices = new HashMap<>(numVertices);
    }

    public Boolean existeEtiqueta(E origen, E destino) throws Exception {
        return this.existeArista(obtenerCodigoEtiqueta(origen), obtenerCodigoEtiqueta(destino));
    }

    public void insertarAristaEtiqueta(E origen, E destino, Float peso) throws Exception {
        insertarArista(obtenerCodigoEtiqueta(origen), obtenerCodigoEtiqueta(destino), peso);
    }

    public void insertarAristaEtiqueta(E origen, E destino) throws Exception {
        insertarArista(obtenerCodigoEtiqueta(origen), obtenerCodigoEtiqueta(destino));
    }

    public ListaEnlazada<Adyacencia> adyacentesEtiquetas(E origen) {
        return adyacentes(obtenerCodigoEtiqueta(origen));
    }

    private Integer obtenerCodigoEtiqueta(E etiqueta) {
        return dicVertices.get(etiqueta);
    }

    public E obtenerEtiqueta(Integer codigo) {
        return etiquetas[codigo];
    }

    public void etiquetarVertice(Integer codigo, E etiqueta) {
        etiquetas[codigo] = etiqueta;
        dicVertices.put(etiqueta, codigo);
    }

    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder();
        try {
            for (int i = 1; i <= numVertices(); i++) {
                grafo.append("Vertice " + obtenerEtiqueta(i));
                ListaEnlazada<Adyacencia> lista = adyacentes(i);
                for (int j = 0; j < lista.getTamanio(); j++) {
                    Adyacencia a = lista.obtenerPosicion(j);
                    if (a.getPeso().toString().equalsIgnoreCase(String.valueOf(Float.NaN))) {
                        grafo.append("<-------->Vertice destino " + obtenerEtiqueta(a.getDestino()) + "---SP ");
                    } else {
                        grafo.append("<------->Vertice destino " + obtenerEtiqueta(a.getDestino()) + "---Peso " + a.getPeso() + " km");

                    }
                }
                grafo.append("\n");
            }
        } catch (Exception e) {
            grafo.append(e.getMessage());
        }

        return grafo.toString();
    }
}
