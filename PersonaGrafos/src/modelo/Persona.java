package modelo;

import modelo.enums.Genero;

/**
 * fecha: 29/01/2023
 *
 * @author: CEAS
 */
public class Persona {

    private Integer id, edad;
    private String nombre;
    private Genero genero;
    private Ubicacion ubicacion;

    public Persona() {
        ubicacion = new Ubicacion();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return nombre+" Lat: "+ubicacion.getLatitud()+" Long: "+ubicacion.getLongitud();
    }
    
}
