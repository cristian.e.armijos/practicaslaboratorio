package vista;

import controlador.ControladorAvion;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import controlador.listas.exceptions.AtributoException;
import javax.swing.JOptionPane;
import vista.tabla.ModeloTabla;

/**
 * fecha: 26/12/2022
 *
 * @author: CEAS
 */
public class VistaDatos extends javax.swing.JDialog {

    ModeloTabla modeloTabla = new ModeloTabla();
    ControladorAvion controladorAvion = new ControladorAvion();

    public VistaDatos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    private void cargarTabla(ListaEnlazada lista) {
        modeloTabla.setLista(lista);
        modeloTabla.fireTableDataChanged();
        tblTabla.setModel(modeloTabla);
        tblTabla.updateUI();
    }

    private void buscar() {
        try {
            ListaEnlazada lista =  new ListaEnlazada();
            if (cbxTipoBusqueda.getSelectedIndex() == 0) {
             lista=  controladorAvion.getLista().binaria(txtBuscar.getText(), cbxAtributo.getSelectedItem().toString());

            } else if (cbxTipoBusqueda.getSelectedIndex() == 1) {
               lista= controladorAvion.getLista().secuencialBinaria(txtBuscar.getText(), cbxAtributo.getSelectedItem().toString());
            }
            if (lista.getTamanio() > 0) {
                cargarTabla(lista);
            } else {
                JOptionPane.showMessageDialog(null, "El elemento buscado no existe", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
            }

        } catch (Exception ex) {
            System.out.println("Error no se pudo buscar " + ex.getMessage());
        }
    }

    private void ordenar() {
        try {
            ListaEnlazada lista = controladorAvion.getLista().quickSort(controladorAvion.getLista(), cbxAtributo.getSelectedItem().toString());
            cargarTabla(lista);
            controladorAvion.setLista(lista);
        } catch (ListaNulaException | PosicionNoEncontradaException | IllegalArgumentException | IllegalAccessException | AtributoException ex) {
            JOptionPane.showMessageDialog(null, "Error al ordenar " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void limpiar() {
        txtFabricante.setText("");
        cbxTipo.setSelectedIndex(0);
        txtNombreAvion.setText("");
        spinnerAnio.setValue(2022);
        spinnerCapacidad.setValue(1);
        controladorAvion.setAvion(null);
    }

    private void guardar() {
        if (txtFabricante.getText().trim().isEmpty() || txtNombreAvion.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            controladorAvion.getAvion().setFabricante(txtFabricante.getText().trim());
            controladorAvion.getAvion().setNombreAvion(txtNombreAvion.getText().trim());
            controladorAvion.getAvion().setTipo(cbxTipo.getSelectedItem().toString());
            controladorAvion.getAvion().setCapacidad(Integer.valueOf(spinnerCapacidad.getValue().toString()));
            controladorAvion.getAvion().setAnioFabricacion(Integer.valueOf(spinnerAnio.getValue().toString()));
            controladorAvion.getAvion().setId(modeloTabla.getRowCount() + 1);
            controladorAvion.getLista().insertar(controladorAvion.getAvion());

            if (controladorAvion.guardar(controladorAvion.getAvion())) {
                JOptionPane.showMessageDialog(null, "Se ha guardado", "OK", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "No se guardo", "Error", JOptionPane.ERROR_MESSAGE);
            }
            cargarTabla(controladorAvion.getLista());
        }
        limpiar();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombreAvion = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        spinnerAnio = new javax.swing.JSpinner();
        spinnerCapacidad = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        txtFabricante = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        cbxTipo = new javax.swing.JComboBox<>();
        cbxTipoBusqueda = new javax.swing.JComboBox<>();
        txtBuscar = new javax.swing.JTextField();
        cbxAtributo = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, 500, 150));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Datos"));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel1.setText("Año fabricación");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 20, -1, -1));
        jPanel2.add(txtNombreAvion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 180, -1));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel5.setText("Capacidad pasajeros");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 20, -1, -1));

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel6.setText("Tipo");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 90, -1, -1));

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel7.setText("Nombre avión");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        spinnerAnio.setModel(new javax.swing.SpinnerNumberModel(2022, 2000, 2023, 1));
        jPanel2.add(spinnerAnio, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 40, 70, -1));

        spinnerCapacidad.setModel(new javax.swing.SpinnerNumberModel(1, 1, 700, 100));
        jPanel2.add(spinnerCapacidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, -1, -1));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel8.setText("Fabricante");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));
        jPanel2.add(txtFabricante, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 180, -1));

        btnGuardar.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel2.add(btnGuardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 110, 90, 30));

        cbxTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MILITAR", "COMERCIAL", "CARGA" }));
        jPanel2.add(cbxTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 110, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 500, 170));

        cbxTipoBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "BINARIA", "SECUENCIAL-BINARIA" }));
        cbxTipoBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxTipoBusquedaActionPerformed(evt);
            }
        });
        jPanel1.add(cbxTipoBusqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 220, 170, -1));

        txtBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarActionPerformed(evt);
            }
        });
        jPanel1.add(txtBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 220, 170, -1));

        cbxAtributo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TIPO", "FABRICANTE", "NOMBREAVION", "ANIOFABRICACION", "CAPACIDAD", "ID" }));
        cbxAtributo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxAtributoActionPerformed(evt);
            }
        });
        jPanel1.add(cbxAtributo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 130, -1));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel2.setText("Buscar");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 200, -1, -1));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel3.setText("Atributo");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel4.setText("Tipo búsqueda");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 200, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cbxTipoBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxTipoBusquedaActionPerformed
    }//GEN-LAST:event_cbxTipoBusquedaActionPerformed

    private void txtBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBuscarActionPerformed
        if (" ".equals(txtBuscar.getText()) || "".equals(txtBuscar.getText())) {
            cargarTabla(controladorAvion.getLista());
        } else {
            buscar();
        }
    }//GEN-LAST:event_txtBuscarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        guardar();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void cbxAtributoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxAtributoActionPerformed
        ordenar();
    }//GEN-LAST:event_cbxAtributoActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaDatos dialog = new VistaDatos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JComboBox<String> cbxAtributo;
    private javax.swing.JComboBox<String> cbxTipo;
    private javax.swing.JComboBox<String> cbxTipoBusqueda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner spinnerAnio;
    private javax.swing.JSpinner spinnerCapacidad;
    private javax.swing.JTable tblTabla;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtFabricante;
    private javax.swing.JTextField txtNombreAvion;
    // End of variables declaration//GEN-END:variables

}
