package vista.tabla;

import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import javax.swing.table.AbstractTableModel;
import modelo.Avion;

/**
 * fecha: 25/12/2022
 *
 * @author: CEAS
 */
public class ModeloTabla extends AbstractTableModel {

    private ListaEnlazada<Avion> lista = new ListaEnlazada<>();

    public ListaEnlazada<Avion> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Avion> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.getTamanio();
    }

    @Override
    public int getColumnCount() {

        return 6;
    }

    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "Nro";
            case 1:
                return "Anio fabricacion";
            case 2:
                return "Capacidad";
            case 3:
                return "Tipo";
            case 4:
                return "Nombre avion";
            case 5:
                return "Fabricante";
            default:
                return null;
        }

    }

    @Override
    public Object getValueAt(int fila, int columna) {
        Avion avion = null;
        try {
            avion = lista.obtenerPosicion(fila);
        } catch (ListaNulaException | PosicionNoEncontradaException ex) {
            System.out.println("Error " + ex.getMessage());
        }
        switch (columna) {
            case 0:
                return (avion != null) ? avion.getId() : "";
            case 1:
                return (avion != null) ? avion.getAnioFabricacion() : "";
            case 2:
                return (avion != null) ? avion.getCapacidad() : "";

            case 3:
                return (avion != null) ? avion.getTipo() : "";

            case 4:
                return (avion != null) ? avion.getNombreAvion() : "";

            case 5:
                return (avion != null) ? avion.getFabricante() : "";
            default:
                return null;
        }
    }
}
