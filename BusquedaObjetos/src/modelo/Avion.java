package modelo;

/**
 * fecha: 27/12/2022
 *
 * @author: CEAS
 */
public class Avion{

    private Integer id, anioFabricacion, capacidad;
    private String nombreAvion, fabricante, tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnioFabricacion() {
        return anioFabricacion;
    }

    public void setAnioFabricacion(Integer anioFabricacion) {
        this.anioFabricacion = anioFabricacion;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public String getNombreAvion() {
        return nombreAvion;
    }

    public void setNombreAvion(String nombreAvion) {
        this.nombreAvion = nombreAvion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return  "Id: "+id+"\nAnio fabricacion: " + anioFabricacion + "\nCapacidad: " + capacidad + "\nNombre avion: " + nombreAvion + "\nFabricante: " + fabricante + "\nTipo: " + tipo;
    }

}
