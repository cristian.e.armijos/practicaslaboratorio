package controlador.listas.excepciones;

/**
 * fecha: 9 nov 2022
 * @author: CEAS 
 */
public class ListaNulaException extends Exception{

    public ListaNulaException() {
        super("Lista vacia");
    }

    public ListaNulaException(String msg) {
        super(msg);
    }
}
