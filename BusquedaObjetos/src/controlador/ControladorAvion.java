package controlador;

import controlador.listas.ListaEnlazada;
import java.io.File;
import java.io.FileWriter;
import modelo.Avion;

/**
 * fecha: 27/12/2022
 *
 * @author: CEAS
 */
public class ControladorAvion {

    private static final String DIR = "Data" + File.separatorChar;
    Avion avion;
    ListaEnlazada<Avion> lista;

    public Avion getAvion() {
        if (avion == null) {
            avion = new Avion();
        }
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public ListaEnlazada<Avion> getLista() {
        if (lista == null) {
            lista = new ListaEnlazada<>();
        }
        return lista;
    }

    public void setLista(ListaEnlazada<Avion> lista) {
        this.lista = lista;
    }

    public Boolean guardar(Avion avion) {
        try {
            FileWriter archivo = new FileWriter(DIR + "Avion.txt", true);
            archivo.append(avion.toString() + "\n\n");
            archivo.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return false;
    }

}
