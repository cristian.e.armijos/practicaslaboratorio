package vista.tabla;

import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import javax.swing.table.AbstractTableModel;

/**
 * fecha: 25/12/2022
 *
 * @author: CEAS
 */
public class ModeloTabla extends AbstractTableModel {

    private ListaEnlazada<String> lista = new ListaEnlazada<>();

    public ListaEnlazada<String> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<String> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {

        return lista.getTamanio();
    }

    @Override
    public int getColumnCount() {

        return 2;
    }

    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "Nro";
            case 1:
                return "Datos";
            default:
                return null;
        }

    }

    @Override
    public Object getValueAt(int fila, int columna) {
        switch (columna) {
            case 0:
                return fila+1;
            case 1: {
                try {
                    return (lista != null) ? lista.obtenerPosicion(fila) : "";
                } catch (ListaNulaException | PosicionNoEncontradaException ex) {
                    System.out.println("Error " + ex.getMessage());
                }
            }

            default:
                return null;
        }
    }
}
