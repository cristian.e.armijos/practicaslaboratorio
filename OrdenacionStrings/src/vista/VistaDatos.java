package vista;

import controlador.ControladorDatos;
import controlador.listas.ListaEnlazada;
import javax.swing.JOptionPane;
import vista.tabla.ModeloTabla;

/**
 * fecha: 23/12/2022
 *
 * @author: CEAS
 */
public class VistaDatos extends javax.swing.JDialog {

    ControladorDatos cd = new ControladorDatos();
    ModeloTabla modeloTabla = new ModeloTabla();
    private String[] arreglo;

    public VistaDatos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnAscendente.setEnabled(false);
        btnDescendente.setEnabled(false);
        cbxMetodoOrdenacion.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnCrearDatos = new javax.swing.JButton();
        btnAscendente = new javax.swing.JRadioButton();
        btnDescendente = new javax.swing.JRadioButton();
        cbxMetodoOrdenacion = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        txtTiempo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Vista datos");
        setResizable(false);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 440, 150));

        btnCrearDatos.setText("CREAR DATOS");
        btnCrearDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearDatosActionPerformed(evt);
            }
        });
        jPanel1.add(btnCrearDatos, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, 120, -1));

        btnAscendente.setText("Ascendente");
        btnAscendente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAscendenteActionPerformed(evt);
            }
        });
        jPanel1.add(btnAscendente, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 90, 20));

        btnDescendente.setText("Descendente");
        btnDescendente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDescendenteActionPerformed(evt);
            }
        });
        jPanel1.add(btnDescendente, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 100, 20));

        cbxMetodoOrdenacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "QUICKSORT", "SHELL" }));
        cbxMetodoOrdenacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxMetodoOrdenacionActionPerformed(evt);
            }
        });
        jPanel1.add(cbxMetodoOrdenacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 60, -1, -1));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel1.setText("Tiempo");
        jLabel1.setAlignmentX(0.5F);
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 70, 40));

        txtTiempo.setFont(new java.awt.Font("Century", 1, 18)); // NOI18N
        jPanel1.add(txtTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 16, 130, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 265, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void ordenar() {
        if (cbxMetodoOrdenacion.getSelectedIndex() == 0) {
            if (btnDescendente.isSelected()) {
                arreglo = cd.ordenar(cbxMetodoOrdenacion.getSelectedItem().toString(), btnDescendente.getText(), arreglo);

            } else {
                arreglo = cd.ordenar(cbxMetodoOrdenacion.getSelectedItem().toString(), btnAscendente.getText(), arreglo);
            }
        } else if (cbxMetodoOrdenacion.getSelectedIndex() == 1) {
            if (btnDescendente.isSelected()) {
                arreglo = cd.ordenar(cbxMetodoOrdenacion.getSelectedItem().toString(), btnDescendente.getText(), arreglo);
            } else {
                arreglo = cd.ordenar(cbxMetodoOrdenacion.getSelectedItem().toString(), btnAscendente.getText(), arreglo);
            }
        }
        cargarTabla();
    }

    private void cargarTabla() {
        modeloTabla.setLista(new ListaEnlazada<String>().toLista(arreglo));
        tblTabla.setModel(modeloTabla);
        tblTabla.updateUI();
    }

    private void activarBotones() {
        btnAscendente.setEnabled(true);
        btnDescendente.setEnabled(true);
        cbxMetodoOrdenacion.setEnabled(true);

    }

    private void medirTiempo(Long tiempo) {
        txtTiempo.setText(tiempo.toString() + " ms");
    }


    private void btnCrearDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearDatosActionPerformed
        arreglo = cd.getDatos().crearArreglo();

        JOptionPane.showMessageDialog(null, "Datos creados", "Ok", JOptionPane.INFORMATION_MESSAGE);
        activarBotones();
    }//GEN-LAST:event_btnCrearDatosActionPerformed

    private void btnDescendenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDescendenteActionPerformed
        if (btnDescendente.isSelected()) {
            btnAscendente.setSelected(false);
            Long inicio = System.currentTimeMillis();
            ordenar();
            Long fin = System.currentTimeMillis();
            medirTiempo(fin-inicio);

        }

    }//GEN-LAST:event_btnDescendenteActionPerformed

    private void cbxMetodoOrdenacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxMetodoOrdenacionActionPerformed
        Long inicio = System.currentTimeMillis();
        ordenar();
        Long fin = System.currentTimeMillis();
        medirTiempo(fin - inicio);
    }//GEN-LAST:event_cbxMetodoOrdenacionActionPerformed

    private void btnAscendenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAscendenteActionPerformed
        if (btnAscendente.isSelected()) {
            btnDescendente.setSelected(false);
            Long inicio = System.currentTimeMillis();
            ordenar();
            Long fin = System.currentTimeMillis();
            medirTiempo(fin - inicio);
        }
    }//GEN-LAST:event_btnAscendenteActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaDatos dialog = new VistaDatos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton btnAscendente;
    private javax.swing.JButton btnCrearDatos;
    private javax.swing.JRadioButton btnDescendente;
    private javax.swing.JComboBox<String> cbxMetodoOrdenacion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblTabla;
    private javax.swing.JLabel txtTiempo;
    // End of variables declaration//GEN-END:variables

}
