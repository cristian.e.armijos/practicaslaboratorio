package modelo;

/**
 * fecha: 23/12/2022
 *
 * @author: CEAS
 */
public class Datos {

    private final String[] arreglo;

    public Datos() {
        this.arreglo = new String[20000];
    }

    public String[] crearArreglo() {

        String[] nombres = {"Carla", "Sofia", "Jose", "Juan", "Maria", "Pablo", "Jazmin", "Paulina", "Luis", "Hugo", "Rafael", "Tiago", "Wilson", "Brayan", "Santiago",
            "Lisseth", "Wilfrido", "Yomayra", "Ana", "Estefania"};
        String[] apellidos = {"Gonzales", "Malla", "Suarez", "Fernandez", "Namicela", "Herrera", "Vivanco", "Lopez", "Torres", "Puglla", "Quezada", "Gutierrez", "Loja",
            "Suarez", "Vera", "Abrigo", "Cabrera", "Flores", "Hernandez", "Alvarado"};

        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = String.valueOf((int) (Math.random() * 20000));
            if (arreglo[arreglo.length / 2] != null) {
                for (i = (arreglo.length / 2 + 1); i < arreglo.length; i++) {
                    int entero1 = (int) (Math.random() * nombres.length - 1);
                    int entero2 = (int) (Math.random() * apellidos.length - 1);
                    arreglo[i] = nombres[entero1] + " " + apellidos[entero2];
                    if (arreglo.length == i) {
                        break;
                    }
                }
            }
        }

        return arreglo;
    }
}
