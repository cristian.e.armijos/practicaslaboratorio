package controlador;

import modelo.Datos;

/**
 * fecha: 23/12/2022
 *
 * @author: CEAS
 */
public class ControladorDatos {

    private Datos datos;

    public Datos getDatos() {
        if (datos == null) {
            datos = new Datos();
        }
        return datos;
    }

    public void setDatos(Datos datos) {
        this.datos = datos;
    }

    public String[] ordenar(String tipoOrdenacion, String tipo, String[] arreglo) {
        if (tipoOrdenacion.equalsIgnoreCase("Quicksort")) {
            arreglo =  Ordenacion.quickSort(arreglo,0,arreglo.length-1);
            if (tipo.equalsIgnoreCase("Descendente")) {
                arreglo= invertirDatos(arreglo);
            }
            return arreglo;
        } else {
           arreglo= Ordenacion.shell(arreglo);
            if (tipo.equalsIgnoreCase("Descendente")) {
                arreglo = invertirDatos(arreglo);
            }
            return arreglo;
        }
    }

    private String[] invertirDatos(String arreglo[]) {
        String[] array = new String[arreglo.length];
        for (int i = arreglo.length-1, j=0; i >= 0; i--) {
            array[j] = arreglo[i];
            j++;
        }
        return array;
    }
}
