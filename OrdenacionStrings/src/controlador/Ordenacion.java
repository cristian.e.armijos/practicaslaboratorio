package controlador;

/**
 * fecha: 15/12/2022
 *
 * @author: CEAS
 */
public class Ordenacion {

    public static String[] quickSort(String[] arreglo, Integer inicio, Integer fin) {

        String pivote = arreglo[(inicio + fin) / 2];
        Integer i = inicio;
        Integer j = fin;

        while (i <= j) {
            while (arreglo[i].compareTo(pivote) < 0) {
                i++;
            }

            while (pivote.compareTo(arreglo[j]) < 0) {
                j--;
            }
            if (i <= j) {
                String aux = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = aux;
                i++;
                j--;
            }
        }
        if (inicio < j) {
            quickSort(arreglo, inicio, j);
        }
        if (i < fin) {
            quickSort(arreglo, i, fin);
        }
        return arreglo;
    }

    public static String[] shell(String[] arreglo) {
        for (int salto = arreglo.length / 2; salto > 0; salto /= 2) {
            for (int i = salto; i < arreglo.length; i++) {
                Integer j;
                String temporal = arreglo[i];
                for (j = i; j >= salto && arreglo[j - salto].compareTo(temporal) > 0; j -= salto) {
                    arreglo[j] = arreglo[j - salto];
                }
                arreglo[j] = temporal;
            }
        }
        return arreglo;
    }
}
