package controlador.listas;

import controlador.listas.excepciones.ListaNulaException;
import controlador.listas.excepciones.PosicionNoEncontradaException;
import java.lang.reflect.Array;

/**
 * fecha: 9 nov 2022
 *
 * @author: CEAS
 */
public class ListaEnlazada<E> {

    //lista: Colección de nodos = elementos = dato y apuntador
    private NodoLista<E> cabecera;
    private Integer tamanio;
    public static Integer ASCENDENTE = 1;
    public static Integer DESCENDENTE = 2;

    //crear lista
    public ListaEnlazada() {
        cabecera = null;
        tamanio = 0;
    }

    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    public Integer getTamanio() {
        return tamanio;
    }

    public void setTamanio(Integer tamanio) {
        this.tamanio = tamanio;
    }

    //lista esta vacia
    public Boolean estaVacia() {
        return cabecera == null;
    }

    //insertar datos al final
    public void insertar(E dato) {
        NodoLista<E> nodo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            this.cabecera = nodo;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getApuntador() != null) {
                aux = aux.getApuntador();
            }
            aux.setApuntador(nodo);
        }
        tamanio++;
    }

    //imprime datos
    public void imprimir() {
        System.out.println("------------------LISTA ENLAZADA-------------------");
        NodoLista<E> aux = cabecera;
        while (aux != null) {
            System.out.print(aux.getDato().toString() + "\t");
            aux = aux.getApuntador();
        }
        System.out.println("\n=====================================================");
    }

    //insertar datos al inicio de la lista
    public void insertarCabecera(E dato) {
        NodoLista<E> nodo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            insertar(dato);
        } else {
            nodo.setApuntador(cabecera);
            cabecera = nodo;
            tamanio++;
        }
    }

    public void insertarPosicion(E dato, Integer pos) throws PosicionNoEncontradaException {
        if (estaVacia()) {
            insertar(dato);
        } else if (pos >= 0 && pos < tamanio) {
            NodoLista<E> nodo = new NodoLista<>(dato, null);
            NodoLista<E> aux = cabecera;
            if (pos == (tamanio - 1)) {
                insertar(dato);
            } else if (pos == 0) {
                insertarCabecera(dato);
            } else {

                for (int i = 0; i < pos - 1; i++) {
                    aux = aux.getApuntador();
                }
                NodoLista<E> siguiente = aux.getApuntador();
                aux.setApuntador(nodo);
                nodo.setApuntador(siguiente);
                tamanio++;
            }
        } else {
            throw new PosicionNoEncontradaException();
        }
    }

    public E obtenerPosicion(Integer pos) throws ListaNulaException, PosicionNoEncontradaException {
        E dato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < tamanio) {
                if (pos == 0) {
                    dato = cabecera.getDato();

                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getApuntador();
                    }
                    dato = aux.getDato();
                }
            } else {
                throw new PosicionNoEncontradaException();
            }
            return dato;
        } else {
            throw new ListaNulaException();
        }

    }

    public E eliminarDato(Integer pos) throws Exception {
        E dato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < tamanio) {
                if (pos == 0) {
                    dato = cabecera.getDato();
                    cabecera = cabecera.getApuntador();
                    tamanio--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getApuntador();
                    }
                    dato = aux.getDato();
                    NodoLista<E> proximo = aux.getApuntador();
                    aux.setApuntador(proximo.getApuntador());
                    tamanio--;
                }
            } else {
                throw new PosicionNoEncontradaException();
            }
            return dato;
        } else {
            throw new ListaNulaException();
        }
    }

    //método para transformar lista a un arreglo
    public E[] toArray() {
        E[] arreglo = null;
        if (tamanio > 0) {
            arreglo = (E[]) Array.newInstance(cabecera.getDato().getClass(), tamanio);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < tamanio; i++) {
                arreglo[i] = aux.getDato();
                aux = aux.getApuntador();
            }
        }
        return arreglo;
    }

    //método para convertir un arreglo en una lista enlazada
    public ListaEnlazada<E> toLista(E[] arreglo) {
        vaciarLista();
        for (int i = 0; i < arreglo.length; i++) {
            insertar(arreglo[i]);
        }
        return this;
    }

    public void vaciarLista() {
        cabecera = null;
        tamanio=0;
    }
}

   
